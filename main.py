from sklearn.metrics import classification_report, accuracy_score
from seqeval.metrics import classification_report as seqclassify
from datasets import load_dataset
import pycrfsuite


def load_conll2003_dataset():
    dataset = load_dataset('conll2003')
    return dataset['train'], dataset['test'], dataset['validation']


def prepare_data(data):
    sentences = []
    for item in data:
        words = item['tokens']
        labels = item['ner_tags']
        sentences.append((words, labels))
    return sentences


def word_features(sent, i):
    features = {
        'word': sent[i],
        'is_first': i == 0,
        'is_last': i == len(sent) - 1,
        'is_capitalized': sent[i][0].upper() == sent[i][0],
        'is_all_caps': sent[i].upper() == sent[i],
        'is_all_lower': sent[i].lower() == sent[i],
        'prefix_1': sent[i][0],
        'prefix_2': sent[i][:2],
        'prefix_3': sent[i][:3],
        'prefix_4': sent[i][:4],
        'suffix_1': sent[i][-1],
        'suffix_2': sent[i][-2:],
        'suffix_3': sent[i][-3:],
        'suffix_4': sent[i][-4:],
        'prev_word': '' if i == 0 else sent[i-1],
        'next_word': '' if i == len(sent) - 1 else sent[i+1],
    }
    return features


def conll2003_to_iob(entities, encoding):
    return [encoding[e] for e in entities]


def train_model(sentences, label_encoding, c1, c2):
    trainer = pycrfsuite.Trainer(verbose=False)

    for sent, labels in sentences:
        if labels:
            features_in_sentence = [word_features(sent, i) for i in range(len(sent))]
            prediction = conll2003_to_iob(labels, label_encoding)
            trainer.append(features_in_sentence, prediction)

    trainer.set_params({
        'c1': c1,
        'c2': c2,
        'max_iterations': 100,
        'feature.possible_transitions': True
    })

    trainer.train('conll2003.crfsuite')


def predict(tagger, sentence):
    features_in_sentence = [word_features(sentence, i) for i in range(len(sentence))]
    prediction = tagger.tag(features_in_sentence)
    return prediction


def evaluate_model(tagger, test_sentences, label_encoding):
    all_true = []
    all_pred = []

    for sent, labels in test_sentences:
        true_labels = conll2003_to_iob(labels, label_encoding)
        predicted_labels = predict(tagger, sent)
        all_true.append(true_labels)
        all_pred.append(predicted_labels)

    score_report = seqclassify(all_true, all_pred)
    print('\n'.join(score_report.splitlines()))


def main():
    print("All libraries imported successfully.")

    train_data, test_data, validation_data = load_conll2003_dataset()

    train_sentences = prepare_data(train_data)
    test_sentences = prepare_data(test_data)
    validation_sentences = prepare_data(validation_data)

    print("Train Data Example: {}".format(train_sentences[0]))
    print('Dataset Splits - Train: {}, Test: {}, Validation: {}'.format(
        len(train_sentences), len(test_sentences), len(validation_sentences))
    )

    label_encoding = {0: 'O', 1: 'B-PER', 2: 'I-PER', 3: 'B-ORG', 4: 'I-ORG', 5: 'B-LOC', 6: 'I-LOC', 7: 'B-MISC', 8: 'I-MISC'}

    c1s = [0.01, 0.05, 0.1, 0.2]
    c2s = [0.01, 0.05, 0.1, 0.2]
    
    for c1 in c1s:
        for c2 in c2s:
            print("c1 c2 ", c1, c2)
            train_model(train_sentences, label_encoding, c1, c2)
            print('Training Complete...')

            tagger = pycrfsuite.Tagger()
            tagger.open('conll2003.crfsuite')

            evaluate_model(tagger, test_sentences, label_encoding)
    
    for c2 in c2s:
        for c1 in c1s:
            print("c1 c2 ", c1, c2)
            train_model(train_sentences, label_encoding, c1, c2)
            print('Training Complete...')

            tagger = pycrfsuite.Tagger()
            tagger.open('conll2003.crfsuite')

            evaluate_model(tagger, test_sentences, label_encoding)
        


if __name__ == '__main__':
    # C1 : 0.01
    # C2 : 0.1
    main()
