import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

from sklearn.metrics import classification_report
from seqeval.metrics import classification_report as seqclassify
from datasets import load_dataset
import pycrfsuite


class BiLSTMCRF(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(BiLSTMCRF, self).__init__()
        self.hidden_dim = hidden_dim
        self.embedding = nn.Embedding(input_dim, hidden_dim)
        self.lstm = nn.LSTM(hidden_dim, hidden_dim, bidirectional=True)
        self.fc = nn.Linear(hidden_dim * 2, output_dim)
        self.crf = pycrfsuite.Trainer(verbose=False)

    def forward(self, x):
        embedded = self.embedding(x)
        lstm_out, _ = self.lstm(embedded)
        lstm_out = lstm_out.view(len(x), -1)
        logits = self.fc(lstm_out)
        return logits

    def train_crf(self, sentences, label_encoding):
        for sent, labels in sentences:
            if labels:
                features_in_sentence = [word_features(sent, i) for i in range(len(sent))]
                prediction = conll2003_to_iob(labels, label_encoding)
                self.crf.append(features_in_sentence, prediction)

    def predict(self, sentence):
        features_in_sentence = [word_features(sentence, i) for i in range(len(sentence))]
        prediction = self.crf.tag(features_in_sentence)
        return prediction


def load_conll2003_dataset():
    dataset = load_dataset('conll2003')
    return dataset['train'], dataset['test'], dataset['validation']


def prepare_data(data):
    sentences = []
    for item in data:
        words = item['tokens']
        labels = item['ner_tags']
        sentences.append((words, labels))
    return sentences


def word_features(sent, i):
    features = {
        'word': sent[i],
        'is_first': i == 0,
        'is_last': i == len(sent) - 1,
        'is_capitalized': sent[i][0].upper() == sent[i][0],
        'is_all_caps': sent[i].upper() == sent[i],
        'is_all_lower': sent[i].lower() == sent[i],
        'prefix_1': sent[i][0],
        'prefix_2': sent[i][:2],
        'prefix_3': sent[i][:3],
        'prefix_4': sent[i][:4],
        'suffix_1': sent[i][-1],
        'suffix_2': sent[i][-2:],
        'suffix_3': sent[i][-3:],
        'suffix_4': sent[i][-4:],
        'prev_word': '' if i == 0 else sent[i-1],
        'next_word': '' if i == len(sent) - 1 else sent[i+1],
    }
    return features


def conll2003_to_iob(entities, encoding):
    return [encoding[e] for e in entities]


def train_model(sentences, label_encoding):
    model = BiLSTMCRF(2048, 128, len(label_encoding))
    for sent, labels in sentences:
        if labels:
            features_in_sentence = [word_features(sent, i) for i in range(len(sent))]
            prediction = conll2003_to_iob(labels, label_encoding)
            model.crf.append(features_in_sentence, prediction)

    model.crf.set_params({
        'c1': 0.1,
        'c2': 0.01,
        'max_iterations': 100,
        'feature.possible_transitions': True
    })

    model.crf.train('conll2003.crfsuite')


def evaluate_model(model, test_sentences, label_encoding):
    all_true = []
    all_pred = []

    for sent, labels in test_sentences:
        true_labels = conll2003_to_iob(labels, label_encoding)
        predicted_labels = model.predict(sent)
        all_true.append(true_labels)
        all_pred.append(predicted_labels)

    score_report = seqclassify(all_true, all_pred)
    print('\n'.join(score_report.splitlines()))


def main():
    print("All libraries imported successfully.")

    train_data, test_data, validation_data = load_conll2003_dataset()

    train_sentences = prepare_data(train_data)
    test_sentences = prepare_data(test_data)
    validation_sentences = prepare_data(validation_data)

    print("Train Data Example: {}".format(train_sentences[0]))
    print('Dataset Splits - Train: {}, Test: {}, Validation: {}'.format(
        len(train_sentences), len(test_sentences), len(validation_sentences))
    )

    label_encoding = {0: 'O', 1: 'B-PER', 2: 'I-PER', 3: 'B-ORG', 4: 'I-ORG', 5: 'B-LOC', 6: 'I-LOC', 7: 'B-MISC', 8: 'I-MISC'}

    train_model(train_sentences, label_encoding)
    print('Training Complete...')

    model = BiLSTMCRF(2048, 128, len(label_encoding))
    model.crf.open('conll2003.crfsuite')

    evaluate_model(model, test_sentences, label_encoding)


if __name__ == '__main__':
    main()
